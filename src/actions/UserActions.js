import {
  CREATE_USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_ERROR,
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR
} from './types';
import UserService from "../services/UserService";
import AuthHelper from '../helpers/AuthHelper';

export const
loginSuccess = () => {
  return (dispatch) => {
    dispatch({type: LOGIN_USER_SUCCESS});
  };
},
createUser = (name, phoneNumber) => {
  return async (dispatch) => {
    try {
      dispatch({type: CREATE_USER});
      const res = await UserService.createUser(name, phoneNumber);
      if (res && res.status === 201) {
        dispatch({type: CREATE_USER_SUCCESS});
      }
    }
    catch (err) {
      dispatch({type: CREATE_USER_ERROR, payload: 'Something went wrong while registering. Please try again'});
    }
  };
},
login = (phone, code) => {
  return async (dispatch) => {
    try {
      dispatch({type: LOGIN_USER});
      const res = await UserService.login(phone, code);
      if (res && res.status === 200) {
        AuthHelper.saveAuthToken(res.data.token);
        loginSuccess();
      }
    }
    catch (err) {
      dispatch({type: LOGIN_USER_ERROR, payload: 'Something went wrong while logging in. Please try again'})
    }
  };
},
getUser = () => {
  return async (dispatch) => {
    try {
      dispatch({type: FETCH_USER});
      const res = await UserService.getUser();
      if (res && res.status === 200) {
        dispatch({type: FETCH_USER_SUCCESS, payload: res.data});
      }
    }
    catch (err) {
      dispatch({type: FETCH_USER_ERROR, payload: 'Something went wrong while fetching user data'});
    }
  }
};
