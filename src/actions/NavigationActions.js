import { NavigationActions } from 'react-navigation';

export const navigateTo = (screenToNavigate, params) => {
  return (dispatch) => {
    dispatch(NavigationActions.navigate({routeName: screenToNavigate, params}));
  }
};
