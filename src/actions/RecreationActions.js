import {
  CREATE_RECREATION,
  CREATE_RECREATION_SUCCESS,
  CREATE_RECREATION_ERROR,
  FETCH_RECREATIONS,
  FETCH_RECREATIONS_SUCCESS,
  FETCH_RECREATIONS_ERROR
} from './types';
import RecreationService from "../services/RecreationService";

export const
createRecreation = (recreation) => {
  return async (dispatch) => {
    try {
      dispatch({type: CREATE_RECREATION});
      const res = await RecreationService.createRecreation(recreation);
      if (res && res.status === 201) {
        dispatch({type: CREATE_RECREATION_SUCCESS});
      }
    }
    catch (err) {
      dispatch({type: CREATE_RECREATION_ERROR, payload: 'Something went wrong while creating recreation!'});
    }
  };
},
getRecreations = () => {
  return async (dispatch) => {
    try {
      dispatch({type: FETCH_RECREATIONS});
      const res = await RecreationService.getRecreations();
      if (res && res.status === 200) {
        dispatch({type: FETCH_RECREATIONS_SUCCESS, payload: res.data});
      }
    }
    catch (err) {
      dispatch({type: FETCH_RECREATIONS_ERROR, payload: 'Something went wrong while fetching recreations!'});
    }
  };
};
