import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {
  Button,
  FormInput,
  FormValidationMessage
} from 'react-native-elements';
import PropTypes from 'prop-types';
import {styles} from "./styles";

class Confirm extends Component {
  static propTypes = {
    confirm: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.onConfirm = this.onConfirm.bind(this);

    this.state = {confirmationCode: '', errorCc: ''};
  }

  onConfirm() {
    if (this.state.confirmationCode !== '' && this.state.confirmationCode.length === 5) {
      this.setState({errorCc: ''});
      this.props.confirm(this.state.confirmationCode);
    } else {
      this.setState({errorCc: 'Must enter valid confirmation code'});
    }
  }

  render() {
    return (
      <View>
        <Text style={styles.message}>
          Please enter 5-digit CODE that you received in SMS and confirm your registartion
        </Text>
        <FormInput
          placeholder='Confirmation Code'
          onChangeText={confirmationCode => this.setState({confirmationCode})}
        />
        {this.state.errorCc !== '' &&
        <FormValidationMessage labelStyle={styles.errorMessage}>
          {this.state.errorCc}
        </FormValidationMessage>
        }
        <Button
          raised
          buttonStyle={styles.button}
          onPress={this.onConfirm}
          icon={{name: 'check-square-o', type: 'font-awesome'}}
          title='Confirm Registration' />
      </View>
    )
  }
}

export default Confirm;
