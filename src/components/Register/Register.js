import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  ToastAndroid
} from 'react-native';
import {
  Button,
  FormLabel,
  FormInput,
  FormValidationMessage
} from 'react-native-elements';
import {createUser, login, getUser} from '../../actions';
import DeviceHelper from '../../helpers/DeviceHelper';
import Confirm from './Confirm';
import {styles} from './styles';

class Register extends Component {
  static navigationOptions = {
    title: 'Register'
  };

  constructor(props) {
    super(props);
    this.onRegisterUser = this.onRegisterUser.bind(this);
    this.onConfirmRegistration = this.onConfirmRegistration.bind(this);
    this.state = {
      text: '',
      error: '',
      errorPhone: '',
      phoneNumber: DeviceHelper.getPhoneNumber(),
      showConfirm: false
    };
  }

  onRegisterUser = async () => {
    this.setState({errorName: '', errorPhone: ''});
    if (this.state.text && this.state.phoneNumber) {
      await this.props.createUser(this.state.text, this.state.phoneNumber);
      if (!this.props.creationError) {
        this.setState({showConfirm: true});
        ToastAndroid.show(`Successfully registered`, ToastAndroid.SHORT);
      } else {
        ToastAndroid.show(this.props.creationError, ToastAndroid.LONG);
      }
    } else if (!this.state.text) {
      this.setState({errorName: 'Must enter your name'});
    } else {
      this.setState({errorPhone: 'Must enter your phone number'});
    }
  };

  onConfirmRegistration = async (confirmationCode) => {
    await this.props.login(this.state.phoneNumber, confirmationCode);
    if (!this.props.loginError) {
      await this.props.getUser();
      if (!this.props.fetchUserError) {
        ToastAndroid.show(`Successfully logged in`, ToastAndroid.LONG);
      } else {
        ToastAndroid.show(this.props.fetchUserError, ToastAndroid.LONG);
      }
    } else {
      ToastAndroid.show(this.props.loginError, ToastAndroid.LONG);
    }
  };

  renderPage() {
    if (this.state.showConfirm) {
      return <Confirm confirm={this.onConfirmRegistration} />
    }
    return (
      <View>
        <FormLabel labelStyle={styles.label}>Name</FormLabel>
        <FormInput
          placeholder='Enter your full name or nickname to register...'
          onChangeText={text => this.setState({text})}
        />
        {this.state.errorName !== '' &&
        <FormValidationMessage labelStyle={styles.errorMessage}>
          {this.state.errorName}
        </FormValidationMessage>
        }
        <FormLabel labelStyle={styles.label}>Phone</FormLabel>
        <FormInput
          placeholder='Enter your phone number...'
          value={this.state.phoneNumber}
          onChangeText={phoneNumber => this.setState({phoneNumber})}
        />
        {this.state.errorPhone !== '' &&
        <FormValidationMessage labelStyle={styles.errorMessage}>
          {this.state.errorPhone}
        </FormValidationMessage>
        }
        <Button
          raised
          buttonStyle={styles.button}
          onPress={this.onRegisterUser}
          icon={{name: 'user-plus', type: 'font-awesome'}}
          title='Register' />
      </View>
    );
  }

  render() {
    return (
      <View>
        {this.renderPage()}
      </View>
    );
  }
}

const mapStateToProps = ({userReducer}) => {

  return {creationError, loginError, fetchUserError} = userReducer;
};

export default connect(mapStateToProps, {createUser, login, getUser})(Register);
