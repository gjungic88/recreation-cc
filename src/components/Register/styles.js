import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  button: {
    backgroundColor: '#0097EF'
  },
  errorMessage: {
    marginTop: -5,
    paddingBottom: 10
  },
  label: {
    fontSize: 18
  },
  message: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
