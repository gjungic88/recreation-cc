import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons'

class RecreationDetails extends Component {
  static navigationOptions = {
    title: 'Recreation Details'
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {name, description, type, number} = this.props.navigation.state.params;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontSize: 20}}>{name}</Text>
        <Text style={{fontSize: 18}}>{type}</Text>
        <Text style={{fontSize: 18}}>{description}</Text>
        <Text style={{fontSize: 18}}>{number}</Text>
        <ActionButton buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item buttonColor='#9b59b6' title="Termin" onPress={() => console.log("notes tapped!")}>
            <Icon name="md-football" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#3498db' title="Member" onPress={() => console.log('create mamber')}>
            <Icon name="md-person-add" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});

export default RecreationDetails;
