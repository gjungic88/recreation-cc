import React, {Component} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import ActionButton from 'react-native-action-button';
import {Card, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {connect} from 'react-redux';
import {navigateTo, getRecreations} from "../../actions";
import footballImg from '../../resources/img/football.png';
import basketballImg from '../../resources/img/basketball.png';

class RecreationList extends Component {
  static navigationOptions = {
    title: 'Recreations',
    headerLeft: null
  };

  componentDidMount() {
    this.props.getRecreations();
  }

  renderRecreationList() {
    if (this.props.recreations) {
      return this.props.recreations.map((recreation, i) => {
        return (
          <Card
            key={i}
            title={recreation.name}
            image={recreation.type === 'FOOTBALL' ? footballImg : basketballImg}>
            <Text style={{marginBottom: 10}}>
              {recreation.description}
            </Text>
            <Button
              icon={{name: 'info-circle', type: 'font-awesome'}}
              onPress={() => this.props.navigateTo('RecreationDetails', recreation)}
              backgroundColor='#03A9F4'
              fontFamily='Lato'
              buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
              title='View Details' />
          </Card>
        );
      })
    }
  }

  render() {
    return (
      <View>
        <ScrollView>
          {this.renderRecreationList()}
        </ScrollView>
        <ActionButton buttonColor="rgba(231,76,60,1)" onPress={() => this.props.navigateTo('CreateRecreation')} />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  welcome: {
    fontSize: 22,
    textAlign: 'center',
    margin: 10,
  },
  recreations: {
    fontSize: 18,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#0097EF',
  },
  buttonContainer: {
    marginTop: 10,
    marginBottom: 5
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: '#FFF',
  }
});

const mapStateToProps = ({recreationReducer}) => {
  const {recreations} = recreationReducer;
  return {
    recreations
  }
};

export default connect(mapStateToProps, {navigateTo, getRecreations})(RecreationList);
