import React, {Component} from 'react';
import {View, Text, Picker, StyleSheet, ScrollView} from 'react-native';
import {
  Button,
  FormLabel,
  FormInput,
  FormValidationMessage
} from 'react-native-elements';
import {connect} from 'react-redux';
import {createRecreation, getRecreations} from "../../actions";

class CreateRecreation extends Component {
  static navigationOptions = {
    title: 'Create Recreation'
  };

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      name: '',
      errorName: '',
      description: '',
      errorDescription: '',
      type: '',
      errorType: '',
      number: '',
      errorNumber: '',
      validationError: ''
    };
  }

  renderPickerItems() {
    return this.props.sportTypes.map((sportType, i) => {
      const {label, value} = sportType;
      return (
        <Picker.Item key={i} label={label} value={value} />
      );
    })
  }

  onSubmit() {
    this.setState({errorName: '', errorDescription: '', errorType: '', errorNumber: ''});
    const {name, description, type, number} = this.state;
    if (!name) {
      this.setState({errorName: 'Must enter recreation name'});
    }
    else if (!description) {
      this.setState({errorDescription: 'Must enter recreation description'});
    }
    else if (!type) {
      this.setState({errorType: 'Must choose recreation sport type'});
    }
    else if (!number) {
      this.setState({errorNumber: 'Must enter recreation members number'});
    }
    else if (isNaN(number)) {
      this.setState({errorNumber: 'Must enter valid members number'});
    }
    else {
      const {loggedIn, userId} = this.props;
      if (loggedIn && userId) {
        this.props.createRecreation({name, description, type, number: parseInt(number), owner: userId});
        this.props.getRecreations();
        this.props.navigation.goBack(this.props.navigation.state.key);
      }
    }
  }

  render() {
    const {errorName, errorDescription, errorType, errorNumber} = this.state;
    const {errorMessage} = styles;
    return (
      <View style={styles.container}>
        <ScrollView>
          <Text style={{textAlign: 'center', fontSize: 18}}>Create Your Own Recreation</Text>
          <FormLabel style={styles.label}>Name</FormLabel>
          <FormInput
            placeholder='Enter recreation name...'
            onChangeText={name => this.setState({name})}
          />
          {errorName !== '' &&
          <FormValidationMessage labelStyle={errorMessage}>
            {errorName}
          </FormValidationMessage>
          }
          <FormLabel style={styles.label}>Description</FormLabel>
          <FormInput
            placeholder='Enter recreation description...'
            onChangeText={description => this.setState({description})}
          />
          {errorDescription !== '' &&
          <FormValidationMessage labelStyle={errorMessage}>
            {errorDescription}
          </FormValidationMessage>
          }
          <FormLabel style={styles.label}>Type</FormLabel>
          <Picker style={styles.picker}
            selectedValue={this.state.type}
            onValueChange={(itemValue, itemIndex) => this.setState({type: itemValue})}>
            {this.renderPickerItems()}
          </Picker>
          {errorType !== '' &&
          <FormValidationMessage labelStyle={errorMessage}>
            {errorType}
          </FormValidationMessage>
          }
          <FormLabel style={styles.label}>Number</FormLabel>
          <FormInput
            placeholder='Enter members number...'
            onChangeText={number => this.setState({number})}
          />
          {errorNumber !== '' &&
          <FormValidationMessage labelStyle={errorMessage}>
            {errorNumber}
          </FormValidationMessage>
          }
          <Button
            raised
            buttonStyle={styles.button}
            onPress={this.onSubmit}
            icon={{name: 'check-square-o', type: 'font-awesome'}}
            title='Create'
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#fff',
    margin: 20
  },
  label: {
    fontSize: 18
  },
  picker: {
    marginLeft: 15
  },
  errorMessage: {
    marginTop: -5,
    paddingBottom: 10
  }
});

const mapStateToProps = ({recreationReducer, userReducer}) => {
  const {loading, sportTypes} = recreationReducer;
  const {loggedIn, id} = userReducer;
  return {
    loading,
    sportTypes,
    loggedIn,
    userId: id
  };
};

export default connect(mapStateToProps, {createRecreation, getRecreations})(CreateRecreation);
