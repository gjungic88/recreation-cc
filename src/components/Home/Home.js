import React, { Component } from 'react';
import {
  View,
} from 'react-native';
import {styles} from "./styles";
import {connect} from 'react-redux';
import {getUser, navigateTo} from "../../actions";
import AuthHelper from '../../helpers/AuthHelper';
import Loader from '../Loader';

class Home extends Component {
  static navigationOptions = {
    header: false
  };

  componentDidMount = async () => {
    const foundToken = await AuthHelper.getAuthToken();
    if (foundToken) {
      this.props.getUser();
    } else {
      this.props.navigateTo('Register');
    }
  };

  componentWillReceiveProps(nextProps) {
    if (!nextProps.loading && nextProps.loggedIn) {
      this.props.navigateTo('RecreationList');
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Loader message='Please Wait' />
      </View>
    );
  }

}

const mapStateToProps = ({userReducer}) => {
  return {
    loggedIn,
    loading
  } = userReducer;
};

export default connect(mapStateToProps, {getUser, navigateTo})(Home);
