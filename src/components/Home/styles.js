import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  welcome: {
    fontSize: 22,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    backgroundColor: '#0097EF',
  },
  buttonContainer: {
    marginTop: 10,
    marginBottom: 5
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: '#FFF',
  }
});
