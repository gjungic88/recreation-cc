import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';

const Loader = ({message, size, color}) => {
  return (
    <View>
      <Text style={{fontSize: 20}}>{message}</Text>
      <ActivityIndicator
        animating={true}
        size={size || 'large'}
        color={color || '#0097EF'}
      />
    </View>
  )
};

export default Loader;
