import {
  CREATE_RECREATION,
  CREATE_RECREATION_SUCCESS,
  CREATE_RECREATION_ERROR,
  FETCH_RECREATIONS,
  FETCH_RECREATIONS_SUCCESS,
  FETCH_RECREATIONS_ERROR
} from "../actions/types";

const INITIAL_STATE = {
  recreations: [],
  sportTypes: [
    {label: 'Select type', value: ''},
    {label: 'Football', value: 'FOOTBALL'},
    {label: 'Basketball', value: 'BASKETBALL'}
  ],
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_RECREATION:
    case FETCH_RECREATIONS:
      return {...state, loading: true};
    case CREATE_RECREATION_SUCCESS:
    case CREATE_RECREATION_ERROR:
    case FETCH_RECREATIONS_ERROR:
      return {...state, loading: false};
    case FETCH_RECREATIONS_SUCCESS:
      return {...state, loading: false, recreations: action.payload};
    default: return state;
  }
}
