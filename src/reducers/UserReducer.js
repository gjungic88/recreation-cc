import {
  CREATE_USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_ERROR,
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR
} from '../actions/types';

const INITIAL_STATE = {
  id: '',
  name: '',
  phone: '',
  loading: false,
  creationError: '',
  loginError: '',
  fetchUserError: '',
  loggedIn: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CREATE_USER:
    case LOGIN_USER:
    case FETCH_USER:
      return {...state, loading: true, creationError: '', loginError: '', fetchUserError: ''};
    case CREATE_USER_SUCCESS:
    case LOGIN_USER_SUCCESS:
      return {...state, loading: false};
    case CREATE_USER_ERROR:
      return {...state, loading: false, creationError: action.payload};
    case LOGIN_USER_ERROR:
      return {...state, loading: false, loginError: action.payload};
    case FETCH_USER_SUCCESS: {
      const {id, name, phone} = action.payload;
      return {...state, id, name, phone, loggedIn: true, loading: false};
    }
    case FETCH_USER_ERROR:
      return {...state, loading: false, fetchUserError: action.payload};
    default: return state;
  }
};
