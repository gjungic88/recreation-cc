import {combineReducers} from 'redux';
import UserReducer from './UserReducer';
import NavReducer from './NavReducer';
import RecreationReducer from './RecreationReducer';
import {createStore, applyMiddleware, compose} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const middleware = applyMiddleware(thunk);
const reducers = combineReducers({
  userReducer: UserReducer,
  recreationReducer: RecreationReducer,
  navReducer: NavReducer
});

export const AppStore = createStore(reducers, __DEV__ ? composeWithDevTools(middleware) : compose(middleware));
