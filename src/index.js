import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {AppStore} from "./reducers";
import AppNavigationWithState from './App';

class Root extends Component {
  render() {
    return (
      <Provider store={AppStore}>
        <AppNavigationWithState />
      </Provider>
    );
  }
}

export default Root;
