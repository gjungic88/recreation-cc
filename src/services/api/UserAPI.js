import client from '../../helpers/AxiosHelper';

const REGISTER_PATH = '/register';
const LOGIN_PATH = '/login';
const GET_USER = '/me';

export default UserAPI = {
  createUser: async (name, phone) => {
    return await client.post(REGISTER_PATH, {name, phone});
  },
  login: async (phone, code) => {
    return await client.post(LOGIN_PATH, {phone, code});
  },
  getUser: async () => {
    return await client.get(GET_USER);
  }
};
