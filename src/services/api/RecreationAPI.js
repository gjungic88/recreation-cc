import client from '../../helpers/AxiosHelper';

const RECREATIONS = '/recreations';

export default RecreationAPI = {
  createRecreation: async (recreation) => {
    return await client.post(RECREATIONS, recreation);
  },
  getRecreations: async () => {
    return await client.get(RECREATIONS);
  },
  getRecreationById: async (recreationId) => {
    return await client.get(`${RECREATIONS}/${recreationId}`);
  },
  updateRecreation: async (recreation) => {
    return await client.put(`${RECREATIONS}/${recreation.id}`, recreation);
  },
  deleteRecreation: async (recreationId) => {
    return await client.delete(`${RECREATIONS}/${recreationId}`);
  }
};
