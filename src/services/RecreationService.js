import RecreationAPI from './api/RecreationAPI';

export default RecreationService = {
  createRecreation: async (recreation) => {
    return await RecreationAPI.createRecreation(recreation);
  },
  getRecreations: async () => {
    return await RecreationAPI.getRecreations();
  },
  getRecreationById: async (recreationId) => {
    return await RecreationAPI.getRecreationById(recreationId);
  },
  updateRecreation: async (recreation) => {
    return await RecreationAPI.updateRecreation(recreation);
  },
  deleteRecreation: async (recreationId) => {
    return await RecreationAPI.deleteRecreation(recreationId);
  }
};
