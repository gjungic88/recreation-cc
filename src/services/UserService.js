import UserAPI from './api/UserAPI';

export default UserService = {
  createUser: async (name, phoneNumber) => {
    return await UserAPI.createUser(name, phoneNumber);
  },
  getUser: async () => {
    return await UserAPI.getUser();
  },
  login: async (phone, code) => {
    return await UserAPI.login(phone, code);
  }
};
