import {StackNavigator} from 'react-navigation';
import {routes} from "./routes";

export default AppNavigator = StackNavigator(routes);
