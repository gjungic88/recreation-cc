import Home from '../components/Home/Home';
import Register from '../components/Register/Register';
import CreateRecreation from '../components/Recreation/CreateRecreation';
import RecreationList from '../components/Recreation/RecreationList';
import RecreationDetails from '../components/Recreation/RecreationDetails';

export const routes = {
  Home: {screen: Home},
  Register: {screen: Register},
  CreateRecreation: {screen: CreateRecreation},
  RecreationList: {screen: RecreationList},
  RecreationDetails: {screen: RecreationDetails}
};
