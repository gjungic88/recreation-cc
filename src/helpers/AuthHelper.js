import SInfo from 'react-native-sensitive-info';

export default AuthHelper = {
  saveAuthToken: async (token) => {
    try {
      SInfo.setItem('auth_token', token, {});
    } catch (error) {
      Promise.reject(error);
    }
  },
  getAuthToken: async () => {
    try {
      const found = await SInfo.getItem('auth_token', {});
      return found;
    } catch (error) {
      return null;
    }
  },
  removeAuthToken: async () => {
    try {
      SInfo.deleteItem('auth_token', {});
    } catch (error) {
      return null;
    }
  }
};
