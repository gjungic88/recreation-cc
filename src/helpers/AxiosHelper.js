import axios from 'axios';
import AuthHelper from '../helpers/AuthHelper';

const baseURL = 'https://cc-recreation.herokuapp.com';
const client = axios.create({baseURL});

client.interceptors.request.use(async request => {
  const auth_token = await AuthHelper.getAuthToken();
  if (auth_token) {
    request.headers.Authorization = `Bearer ${auth_token}`;
  }
  return request;
});

export default client;
