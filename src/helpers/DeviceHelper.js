import DeviceInfo from 'react-native-device-info';

export default DeviceHelper = {
  getPhoneNumber: () => {
    const phoneNum = DeviceInfo.getPhoneNumber();
    return phoneNum;
  }
}
