import React, {Component} from 'react';
import { addNavigationHelpers } from 'react-navigation';
import {connect} from 'react-redux';
import AppNavigator from './config/AppNavigator';


class App extends Component {
  render() {
    return (
      <AppNavigator navigation={addNavigationHelpers({
        dispatch: this.props.dispatch,
        state: this.props.nav
      })} />
    );
  }
}

const mapStateToProps = ({navReducer}) => ({
  nav: navReducer
});

export default connect(mapStateToProps)(App);
